### annotate all the svs from TCGA pan-cancer data ####

structural variants are called using [this reference genome](https://github.com/hall-lab/speedseq#reference-genome-and-annotations)

The chromosome names are with out "chr" and have other contig names start with `GL`.  
remove lines with `GL` first.

```{r}
library(dplyr)
load("PCTL.SpeedSeq.filtered.RData")
ls()
sq[1:5, 1:15]

sq<- sq %>% filter(!grepl("GL", Chr1)) %>% filter(!grepl("GL", Chr2))



```


Make GRanges object from the dataframe

```{r}
library(rtracklayer)
library(GenomicRanges)
## make GRangs for two anchors 
anchor1<- sq[,c(5:7,13)]
anchor1<- GRanges(seqnames = anchor1$Chr1, IRanges(start=anchor1$Start1, end=anchor1$End1), strand = anchor1$Strand1)

anchor2<- sq[,c(8:10,14)]
anchor2<- GRanges(seqnames = anchor2$Chr2, IRanges(start=anchor2$Start2, end=anchor2$End2), strand = anchor2$Strand2)

##convert to UCSC style chromosome names (chr1), now it is "1"
library(BSgenome.Hsapiens.UCSC.hg19)
seqlevels(BSgenome.Hsapiens.UCSC.hg19)
# or
Hsapiens

## change the chromosome names from 1 to chr1 etc.
seqlevelsStyle(anchor1) <- "UCSC"
seqlevels(anchor1)<- seqlevels(BSgenome.Hsapiens.UCSC.hg19)
seqlengths(anchor1)<- seqlengths(BSgenome.Hsapiens.UCSC.hg19)

seqlevelsStyle(anchor2) <- "UCSC"
seqlevels(anchor2)<- seqlevels(BSgenome.Hsapiens.UCSC.hg19)
seqlengths(anchor2)<- seqlengths(BSgenome.Hsapiens.UCSC.hg19)

## add back the meta-info
anchor1$sample_id<- sq$sample_id
anchor1$disease<- sq$disease
anchor1$library_type<- sq$library_type
anchor1$Score<- sq$Score
anchor1$SV_type<- sq$Type

anchor2$sample_id<- sq$sample_id
anchor2$disease<- sq$disease
anchor2$library_type<- sq$library_type
anchor2$Score<- sq$Score
anchor2$SV_type<- sq$Type

```

I made a gencode_hg19 based `txdb` ojbect. gtf file was downloaded from  
`http://www.gencodegenes.org/releases/19.html`
annotate with gencode request here https://github.com/GuangchuangYu/ChIPseeker/issues/28#issuecomment-192860653


```{r}
library(GenomicFeatures)
## Let me use genecode 19 as annotation
genecode.txdb<- makeTxDbFromGFF("~/annotations/human/gencode_hg19.v19/gencode.v19.annotation.gtf.gz", format="gtf",organism = "Homo sapiens", chrominfo = seqinfo(txdb) )

saveDb(genecode.txdb, "~/annotations/human/gencode_hg19.v19/genecode.txdb.sqlite")

```

load the annotation database and annotate with `ChIPseeker`.
Now, `ChIPseeker` support annotating GRangs with the nearest upstream or downstream genes.
see my issue on github here https://github.com/GuangchuangYu/ChIPseeker/issues/17  

and a write-up for low-level implementation by myself here http://crazyhottommy.blogspot.com/2016/01/find-nearest-upstream-genes-using.html

one has to install the devl version of `ChIPseeker`


```{r}
library(GenomicFeatures)
library(devtools)
devtools::install_github("GuangchuangYu/ChIPseeker")
library(ChIPseeker)
gencode.hg19.txdb<- loadDb("genecode.txdb.sqlite")

##takes 11 mins!
anchor1.anno<- annotatePeak(anchor1, tssRegion=c(-3000, 3000), 
                         TxDb= gencode.hg19.txdb, level = "gene", annoDb="org.Hs.eg.db",
                         sameStrand = FALSE, ignoreOverlap = FALSE,
                         ignoreDownstream = TRUE, overlap = "all")

anchor2.anno<- annotatePeak(anchor2, tssRegion=c(-3000, 3000), 
                         TxDb=gencode.hg19.txdb, level = "gene", annoDb="org.Hs.eg.db",
                         sameStrand = FALSE, ignoreOverlap = FALSE,
                         ignoreDownstream = TRUE, overlap = "all")

## pie chart for breakpoints
plotAnnoPie(anchor1.anno)
plotAnnoPie(anchor2.anno)

breakpoints.anno<- inner_join(as.data.frame(anchor1.anno), as.data.frame(anchor2.anno), 
          by=c("sample_id"="sample_id", "Name"="Name", "library_type"="library_type", "disease"="disease"))
          
save(breakpoints.anno, file="PCTL.SV.annotated.rda")
```