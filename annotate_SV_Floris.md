```{r}
library(dplyr)
load("PCTL.SpeedSeq.filtered.RData")
ls()
sq[1:5, 1:15]

sq<- sq %>% filter(!grepl("GL", Chr1)) %>% filter(!grepl("GL", Chr2))

library(rtracklayer)
library(GenomicRanges)
## make GRangs for two anchors 
anchor1<- sq[,c(5:7,13)]
anchor1<- GRanges(seqnames = anchor1$Chr1, IRanges(start=anchor1$Start1, end=anchor1$End1), strand = anchor1$Strand1)

anchor2<- sq[,c(8:10,14)]
anchor2<- GRanges(seqnames = anchor2$Chr2, IRanges(start=anchor2$Start2, end=anchor2$End2), strand = anchor2$Strand2)

##convert to UCSC style chromosome names (chr1), now it is "1"
library(BSgenome.Hsapiens.UCSC.hg19)
seqlevels(BSgenome.Hsapiens.UCSC.hg19)
# or
Hsapiens

## change the chromosome names from 1 to chr1 etc.
seqlevelsStyle(anchor1) <- "UCSC"
seqlevels(anchor1)<- seqlevels(BSgenome.Hsapiens.UCSC.hg19)
seqlengths(anchor1)<- seqlengths(BSgenome.Hsapiens.UCSC.hg19)

seqlevelsStyle(anchor2) <- "UCSC"
seqlevels(anchor2)<- seqlevels(BSgenome.Hsapiens.UCSC.hg19)
seqlengths(anchor2)<- seqlengths(BSgenome.Hsapiens.UCSC.hg19)

## add back the meta-info
anchor1$sample_id<- sq$sample_id
anchor1$disease<- sq$disease
anchor1$library_type<- sq$library_type
anchor1$Score<- sq$Score
anchor1$SV_type<- sq$Type
anchor1$Name<- sq$Name
anchor1$Chr2<- sq$Chr2
anchor1$Start2<- sq$Start2
anchor1$End2<- sq$End2
anchor1$Strand2<- sq$Strand2


anchor2$sample_id<- sq$sample_id
anchor2$disease<- sq$disease
anchor2$library_type<- sq$library_type
anchor2$Score<- sq$Score
anchor2$SV_type<- sq$Type
anchor2$Name<- sq$Name
anchor2$Chr1<- sq$Chr1
anchor2$Start1<- sq$Start1
anchor2$End1<- sq$End1
anchor2$Strand1<- sq$Strand1
```

### doing the intersection with promoter regions (20kb around TSS)

```{r}
library(GenomicFeatures)
library(GenomicRanges)
library(rtracklayer)

gencode.genes<- import.gff("~/annotations/human/gencode_hg19.v19/gencode.v19.annotation.gtf.gz",
                           format="gtf", genome="hg19", feature.type="gene",
                           colnames=c("gene_id", "gene_type","gene_status", "gene_name"))
save(gencode.genes, file="gencode.genes.rda")

## this object is in the same speedseq diretory
load("gencode.genes.rda")

# 20kb will exceed the chromosome limits for some genes, trim them off
gencode.promoters<- trim(promoters(gencode.genes, upstream=20000, downstream=20000))

## the same gene may have mulitple breakpoints
## and a breakpoint may associate with multiple genes
anchor1.hits<- findOverlaps(gencode.promoters, anchor1, ignore.strand=TRUE)

proximal.gene1<- gencode.promoters[queryHits(anchor1.hits)]
anchor1.overlap<- anchor1[subjectHits(anchor1.hits)]
```

### also calculate how far the breakpoint is from the TSS, although findOverlaps will only find the breakpoints within 20kb of TSS, but I want an exact number.

```{r}
## the proximal.gene1 is the promoter region, I will need to get back to the 1bp TSS, otherwise
## the distance will be 0 for all. ideally, need to use gencode.promoters to get back, using resize could lead to 1 base off, but it is fine in this case.

proximal.TSS1<- resize(proximal.gene1, width=1, fix="center")
dists1<- distance(proximal.TSS1, anchor1.overlap, ignore.strand= TRUE)

# dists is the absolute distance between breakpoint and TSS, change to minus if the breakpoint
## is at the 5' of the TSS

minus_or_plus<- function(TSS, breakpoint){
                return ((end(breakpoint) < start(TSS) & as.vector(strand(TSS) == "+")) |
                (start(breakpoint) > start(TSS) & as.vector(strand(TSS) == "-")))
}

# change the distance sign
sdists1<- ifelse(minus_or_plus(proximal.TSS1, anchor1.overlap), -dists1, dists1)

```

### Do the same for anchor2

```{r}
anchor2.hits<- findOverlaps(gencode.promoters, anchor2, ignore.strand = TRUE)

proximal.gene2<- gencode.promoters[queryHits(anchor2.hits)]
anchor2.overlap<- anchor2[subjectHits(anchor2.hits)]

proximal.TSS2<- resize(proximal.gene2, width=1, fix="center")
dists2<- distance(proximal.TSS2, anchor2.overlap, ignore.strand=TRUE)

# change the distance sign
sdists2<- ifelse(minus_or_plus(proximal.TSS2, anchor2.overlap), -dists2, dists2)
```

###Combine data together to a dataframe 

```{r}
# will have duplicated column names, need to tweak the names
proximal.gene.anchor1<- cbind(as.data.frame(proximal.gene1), as.data.frame(anchor1.overlap), distance2TSS = sdists1)

colnames(proximal.gene.anchor1) = c("gene_chr", "gene_start", "gene_end", "window_width", "gene_strand", "gene_id", "gene_type", "gene_status", "gene_name", "ChrProx", "StartProx", "EndProx", "WidthProx", "StrandProx", "sample_id", "disease", "library_type", "Score", "SV_type", "Name", "ChrDist", "StartDist", "EndDist", "StrandDist", "distance2TSS")

proximal.gene.anchor2<- cbind(as.data.frame(proximal.gene2), as.data.frame(anchor2.overlap), distance2TSS = sdists2)

colnames(proximal.gene.anchor2) = c("gene_chr", "gene_start", "gene_end", "window_width", "gene_strand", "gene_id", "gene_type", "gene_status", "gene_name", "ChrProx", "StartProx", "EndProx", "WidthProx", "StrandProx", "sample_id", "disease", "library_type", "Score", "SV_type", "Name", "ChrDist", "StartDist", "EndDist", "StrandDist", "distance2TSS")

sqgene<- rbind(proximal.gene.anchor1, proximal.gene.anchor2)

save(sqgene, file = "sqgene.rda")
```